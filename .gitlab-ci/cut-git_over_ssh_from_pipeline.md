Stuff that's not necessary as it appears that using `--shallow-since` with `git fetch` with GitLab doesn't appear to work unlike regular old `--depth`. It appears `--shallow-since` doesn't work when the remote uses `https` OR `ssh`, however from memory both give different errors making debugging very super not fun. This has been kept here for reference since trying to get SSH working was a pain in the bum and I think my way differed a little bit from the example the code below is mostly based on (I think from GitLab or GitHub docs?). This may be useful if SSH needs to be used for whatever reason or if a pipeline needs to interact with a private repository using git. Note that I don't remember if I was ever able to get this working exactly as I wanted it to for non `--shallow-since` stuff either, so I think it works but it may not and it may need a bit of tweaking, but it should be pretty close to right if not.

```yml
variables:
  GITLAB_ED25519_FINGERPRINT: "AAAAC3NzaC1lZDI1NTE5AAAAIAfuCHKVTjquxvt6CM6tdG4SLp1Btn/nOeHHE5UOzRdf"
<something_requiring_git>:
  stage: <your_stage_name_requiring_git>
  before_script:
    - 'command -v ssh-agent >/dev/null || ( apt-get update -y && apt-get install openssh-client -y )' # Install ssh-agent if not already installed
    - eval $(ssh-agent -s) # Run ssh-agent
    - chmod 400 "$READONLY_DEPLOY_SSH_PRIVATE_KEY" # Give the right permissions, otherwise ssh-add will refuse to add files
    - ssh-add "$READONLY_DEPLOY_SSH_PRIVATE_KEY" # Add the SSH key stored in SSH_PRIVATE_KEY file type CI/CD variable to the agent store
    - mkdir -p ~/.ssh && chmod 700 ~/.ssh # Create the SSH directory and give it the right permissions
    - touch ~/.ssh/known_hosts # Create known_hosts file
    - echo "gitlab.com ssh-ed25519 $GITLAB_ED25519_FINGERPRINT" >> ~/.ssh/known_hosts # Add GitLab's ed25519 ssh fingerprint to known_hosts otherwise git will freak out later (note: personally prefer this over using git with `StrictHostKeyChecking=no`)
```
