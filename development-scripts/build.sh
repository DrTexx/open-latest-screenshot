echo "[BUILD]: Deleting ./dist directory..."
rm -r ./dist
echo "[BUILD]: Injecting version..."
poetry run python ./development-scripts/inject-version.py &&
echo "[BUILD]: Building packages..." &&
poetry run python -m build
