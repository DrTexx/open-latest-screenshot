echo "[GENERATE-HTML-COVERAGE-REPORT]: Running tests and capturing coverage data..."
poetry run coverage run -m pytest tests/ -rs &&
echo "[GENERATE-HTML-COVERAGE-REPORT]: Generating coverage report..." &&
poetry run coverage report &&
echo "[GENERATE-HTML-COVERAGE-REPORT]: Generating HTML coverage report..." &&
poetry run coverage html
