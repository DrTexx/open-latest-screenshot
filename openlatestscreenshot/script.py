# built in
import subprocess
from pathlib import Path
from datetime import datetime
from sys import platform
from typing import Union

# package
from openlatestscreenshot import linux, windows
from openlatestscreenshot.util import handle_new_latest_screenshot

# -----------------
# --- constants ---
# -----------------
DEBUG = False
SUPPORTED_PLATFORMS = ["linux", "win32"]
OPEN_COMMANDS = {"linux": "xdg-open", "win32": "explorer"}
SCREENSHOTS_DIRS = {"linux": "~/Pictures/", "win32": "~/Pictures/Screenshots"}
REPOSITORY_BUG_REPORT_LINK = (
    "https://gitlab.com/DrTexx/open-latest-screenshot/-/issues"
)
IMAGE_FILE_EXTENSIONS = [".png"]


# -----------------
# --- functions ---
# -----------------


def _ensure_environment_compatible(_platform):
    # ensure user is using a supported platform
    if _platform not in SUPPORTED_PLATFORMS:
        raise NotImplementedError(
            f"Incompatible platform: {_platform} (supported platforms: {','.join(SUPPORTED_PLATFORMS)})"
        )


def open_image_in_default_application(image_filepath):
    """Open image at specified filepath using the system's default image viewer."""
    # fetch open command based on platform
    open_command = OPEN_COMMANDS[platform]

    # SEC: ensure we don't try opening any files that aren't images
    # FIXME: Double-check that on Linux that an executable file with a .png extension isn't treated as an executable by xdg-open (web search indicates it's safe, but want to test it myself on a Linux machine)
    if not (image_filepath.suffix in IMAGE_FILE_EXTENSIONS):
        raise RuntimeError(
            f"Fatal: Tried opening a file that wasn't an image as though it were.\n\nPlease file a bug report at {REPOSITORY_BUG_REPORT_LINK}"
        )

    # attempt to open filepath with default image viewer
    try:
        pipes = subprocess.Popen(
            [open_command, image_filepath],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        stdout, stderr = pipes.communicate()

        # if the process we called had a non-zero return code
        if pipes.returncode != 0:
            # HACK: Windows is stupid and returns an exit code of 1 whether it worked or not. Can look into an alternative universally available open command that respects exit codes in the future, but for now it's a low priority.
            if platform == "win32" and pipes.returncode == 1:
                if DEBUG:
                    print(
                        "Warning: Opening image with default Windows app had a return code of 1, but explorer is dumb and does that even when nothing went wrong."
                    )
            else:
                # convert stderr byte array to string and strip newline
                err_msg = stderr.decode()
                # raise specific exception with stderr and request user to open new issue
                raise RuntimeError(
                    f"Failed to open '{image_filepath}' with {open_command}, error received:\n  {err_msg}\n\nPlease file a bug report at {REPOSITORY_BUG_REPORT_LINK}"
                )

    # if 'FileNotFoundError' error is raised
    except FileNotFoundError as err:
        # if error is due to platform's open command not being available
        if err.errno == 2 and err.filename == open_command:
            # raise specific error re. platform's open command not being available
            raise NotImplementedError(
                f"Incompatible system: {open_command} is used to open files on your platform ({platform}), however it is not available on your system."
            ) from err
        # if error is due to a different file not being found
        else:
            # raise error normally
            raise err


def get_screenshot_capture_date(
    filepath: Path, _platform: str = platform
) -> Union[float, None]:
    """Return date a screenshot was taken as UTC POSIX timestamp."""
    if _platform == "linux":
        return linux.get_screenshot_capture_date(filepath)
    elif _platform == "win32":
        return windows.get_screenshot_capture_date(filepath)
    else:
        raise NotImplementedError("Unsupported platform")


def get_latest_screenshot_filepath(
    screenshots_dirpath,
    _platform=platform,
    on_new_latest=None,
):
    """Get filepath to latest screenshot."""
    latest_utc_timestamp = 0.0
    latest_screenshot_filepath = None

    # for each path in screenshots dir
    for filepath in screenshots_dirpath.iterdir():
        # if the path isn't a filepath, skip it
        if not filepath.is_file():
            continue

        # try to get capture date from file
        utc_timestamp = get_screenshot_capture_date(
            filepath, _platform=_platform
        )

        # if a valid screenshot capture date could be retrieved, skip
        if utc_timestamp is None:
            continue

        # if this utc timestamp is more recent than the previously recorded most recent
        if utc_timestamp > latest_utc_timestamp:
            # record timestamp as the most recent found thus far
            latest_utc_timestamp = utc_timestamp
            # record filepath of most recent screenshot found thus far
            latest_screenshot_filepath = filepath
            if on_new_latest:
                on_new_latest(latest_utc_timestamp, latest_screenshot_filepath)

    return latest_screenshot_filepath


def get_screenshot_dirpath(_platform: str = platform) -> Path:
    return Path(SCREENSHOTS_DIRS[_platform]).expanduser()


def open_latest_screenshot():
    """Open the most recently taken screenshot with the system's default image viewer."""
    _ensure_environment_compatible(platform)

    latest_screenshot_filepath = get_latest_screenshot_filepath(
        get_screenshot_dirpath(),
        on_new_latest=handle_new_latest_screenshot if DEBUG else None,
    )

    if DEBUG:
        print(
            f"{latest_screenshot_filepath} is the most recent of all screenshots searched."
        )

    open_image_in_default_application(latest_screenshot_filepath)
