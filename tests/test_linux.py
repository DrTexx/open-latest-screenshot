import pytest
from sys import platform
from pathlib import Path

from openlatestscreenshot import linux


@pytest.mark.skipif(platform != "linux", reason="only runs on Linux")
def test_get_screenshot_capture_date_from_filepath_linux_invalid_filepath():
    """Test when filepath ISN'T valid regex on Linux."""
    invalid_linux_filename = "will not pass regex.png"

    invalid_filename_timestamp = linux.get_screenshot_capture_date(
        Path(invalid_linux_filename)
    )

    assert invalid_filename_timestamp == None


@pytest.mark.skipif(platform != "linux", reason="only runs on Linux")
def test_get_screenshot_capture_date_from_filepath_linux_valid_filepath():
    """Test when filepath IS good on Linux."""
    valid_linux_filename = "Screenshot from 2024-02-27 16-49-12.png"

    valid_filename_timestamp = linux.get_screenshot_capture_date(
        Path(valid_linux_filename)
    )

    assert valid_filename_timestamp == 1709052552.0


@pytest.mark.skipif(platform != "linux", reason="only runs on Linux")
def test_get_screenshot_capture_date_from_filepath_linux_non_image_filepath():
    """Test when filepath ISN'T an image on Linux."""
    non_image_filenames = [
        "Screenshot from 2024-02-27 16-49-12",
        "trust_me_im_a_dolphin.sh",
        "Screenshot from 2024-02-27 16-49-12.png.",
        "Screenshot from 2024-02-27 16-49-12.png.exe",
        "Screenshot from 2024-02-27 16-49-12.scr",
    ]

    for _filename in non_image_filenames:
        valid_filename_timestamp = linux.get_screenshot_capture_date(
            Path(_filename)
        )

        assert valid_filename_timestamp == None
