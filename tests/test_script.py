# built in
import unittest
from unittest.mock import patch
from pathlib import Path

# site
import pytest

# package
from openlatestscreenshot import script, linux, windows
from openlatestscreenshot.util import handle_new_latest_screenshot
from .mocks import MockPath


class test__ensure_environment_compatible(unittest.TestCase):
    def test_compat_test_win32(self):
        """Ensure environment check supports all platforms in SUPPORTED_PLATFORMS."""
        for _platform in script.SUPPORTED_PLATFORMS:
            script._ensure_environment_compatible(_platform)

    def test__ensure_environment_compatible(self):
        """Ensure environment check DOESN'T support a non-sense platform name."""
        with pytest.raises(NotImplementedError):
            script._ensure_environment_compatible("not-a-real-platform")


class test_get_screenshot_capture_date(unittest.TestCase):
    """Test that correct platform-specific functions are called."""

    def setUp(self) -> None:
        self.filepath = Path("bad-path.png")

    def test_windows(self):
        with patch.object(windows, "get_screenshot_capture_date") as mock:
            utc_timestamp = script.get_screenshot_capture_date(
                self.filepath, _platform="win32"
            )

        mock.assert_called_once_with(self.filepath)

    def test_linux(self):
        _platform = "linux"

        with patch.object(linux, "get_screenshot_capture_date") as mock:
            utc_timestamp = script.get_screenshot_capture_date(
                self.filepath, _platform=_platform
            )

        mock.assert_called_once_with(self.filepath)

    def test_incompatible(self):
        _platform = "not-a-real-platform"

        with pytest.raises(NotImplementedError):
            utc_timestamp = script.get_screenshot_capture_date(
                self.filepath, _platform=_platform
            )


class test_get_latest_screenshot_filepath(unittest.TestCase):
    def setUp(self) -> None:
        def test_with(mock_dir_contents, _platform, on_new_latest):
            with patch.object(
                Path,
                "iterdir",
                return_value=mock_dir_contents,
            ):
                return script.get_latest_screenshot_filepath(
                    Path("~/Documents").expanduser(),
                    _platform=_platform,
                    on_new_latest=on_new_latest,
                )

        self.test_with = test_with

    def test_linux(self):
        # NOTE: no need to test what happens if two paths are exactly the same as this should be theoretically impossible and enforced by something in the actual OS
        _mock_dir_contents = [
            MockPath(
                "Screenshot from 2024-02-27 16-49-12.png",
                is_file_override=lambda: True,
            ),
            MockPath(
                "Screenshot from 2024-02-27 16-49-13.png",
                is_file_override=lambda: True,
            ),
            MockPath(
                "Screenshot from 2024-02-27 16-49-14-invalid-regex.png",
                is_file_override=lambda: True,
            ),
            MockPath(
                "Screenshot from 2024-02-27 16-49-15.png",
                is_file_override=lambda: False,
            ),
        ]
        expected_latest_screenshot_filepath = Path(
            "Screenshot from 2024-02-27 16-49-13.png"
        )

        latest_screenshot_filepath = self.test_with(
            mock_dir_contents=_mock_dir_contents,
            _platform="linux",
            on_new_latest=handle_new_latest_screenshot,
        )

        assert (
            latest_screenshot_filepath == expected_latest_screenshot_filepath
        )

    # NOTE: don't think that Windows specific test should be necessary
